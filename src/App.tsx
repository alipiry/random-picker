import React, { Component } from 'react';
import './App.css';

class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
      <div className="App-card">
        <h1>
          Card position
        </h1>
      </div>
        </header>
      </div>
    );
  }
}

export default App;
